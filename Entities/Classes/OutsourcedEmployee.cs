
namespace Entities.Classes
{
   class OutsourcedEmployee : Employee
   {
      public double AdditionalCharge {get; set;}

      public OutsourcedEmployee(double additionalCharge) : base("", 0, 0)
      {
         this.AdditionalCharge = additionalCharge;
      }

      public override double Payment()
      {
         double pay = base.Payment();
         return pay + this.AdditionalCharge + (this.AdditionalCharge / 10); // 110% bonus
      }      
      
      public override string ToString()
      {
         return $"{this.Name} - ${Payment()}";
      }
   }
}