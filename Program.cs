﻿using System;
using System.Collections.Generic;
using Entities.Classes;

class Program
{
   static void Main()
   {
      Console.Write("Enter the number of employees: ");
      int amountEmployees = int.Parse(Console.ReadLine());


      List<Employee> employees = new List<Employee>();
      for (int i = 1; i < amountEmployees +1; i++)
      {
         Console.WriteLine($"Employee #{i} data");

         Console.Write("Outsourced (y/n)? ");
         string answerOutsourced = Console.ReadLine();
         bool isOutsourced = false;
         answerOutsourced = answerOutsourced.ToLower();

         if (answerOutsourced != "y" && answerOutsourced != "n") {
            Console.WriteLine("ERROR: ANSWER NOT VALID");
            return;
         }

         if (answerOutsourced == "y") {
            isOutsourced = true;
         }

         Console.Write("Name: ");
         string name = Console.ReadLine();

         Console.Write("Hours: ");
         int hours = int.Parse(Console.ReadLine());

         Console.Write("Value per hour: ");
         double valuePerHour = double.Parse(Console.ReadLine());

         double additionalCharge = 0;
         if (isOutsourced) {
            Console.Write("Additional charge: ");
            additionalCharge = double.Parse(Console.ReadLine());
         }

         if (isOutsourced == false) {
            var employee = new Employee(name, hours, valuePerHour);
            employees.Add(employee);
         }

         else {
            var employee = new OutsourcedEmployee(additionalCharge);
            employee.Name = name;
            employee.Hours = hours;
            employee.ValuePerHour = valuePerHour;
            employees.Add(employee);
         }
      }

      Console.WriteLine("PAYMENTS:");
      for (int i = 0; i < employees.Count; i++)
      {
         Console.WriteLine(employees[i]);
      }
   }
}